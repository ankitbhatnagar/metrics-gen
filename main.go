package main

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"sync"
	"time"

	"github.com/golang/snappy"
	"github.com/influxdata/influxdb-comparisons/bulk_data_gen/common"
	"github.com/influxdata/influxdb-comparisons/bulk_data_gen/devops"
	"github.com/pkg/errors"
	"github.com/prometheus/common/config"
	"github.com/prometheus/common/version"
	"github.com/prometheus/prometheus/model/labels"
	"github.com/prometheus/prometheus/prompb"
	"go.uber.org/zap"
)

type HostsSimulator interface {
	Hosts() []*devops.Host
	Generate() (map[string][]prompb.TimeSeries, error)
}

var _ HostsSimulator = (*hostsSimulator)(nil)

type hostsSimulator struct {
	sync.RWMutex
	hosts                     []*devops.Host
	appendLabels              []prompb.Label
	syntheticLabelCardinality uint64
}

type HostSimulatorOptions struct {
	Labels                    map[string]string
	SyntheticLabelCardinality uint64
}

func NewHostsSimulator(
	hostCount int,
	start time.Time,
	opts HostSimulatorOptions,
) *hostsSimulator {
	var hosts []*devops.Host
	for i := 0; i < hostCount; i++ {
		host := devops.NewHost(i, 0, start)
		hosts = append(hosts, &host)
	}

	var appendLabels []prompb.Label
	for k, v := range opts.Labels {
		appendLabels = append(appendLabels, prompb.Label{Name: k, Value: v})
	}

	return &hostsSimulator{
		hosts:                     hosts,
		appendLabels:              appendLabels,
		syntheticLabelCardinality: opts.SyntheticLabelCardinality,
	}
}

func (h *hostsSimulator) Hosts() []*devops.Host {
	h.RLock()
	defer h.RUnlock()
	return append([]*devops.Host{}, h.hosts...)
}

func (h *hostsSimulator) Generate() (map[string][]prompb.TimeSeries, error) {
	h.Lock()
	defer h.Unlock()

	now := time.Now()
	hostValues := make(map[string][]prompb.TimeSeries)
	for _, host := range h.hosts {
		allSeries := make([]prompb.TimeSeries, 0, len(host.SimulatedMeasurements))
		for _, measurement := range host.SimulatedMeasurements {
			p := common.MakeUsablePoint()
			measurement.ToPoint(p)

			for i, fieldName := range p.FieldKeys {
				val := 0.0

				switch v := p.FieldValues[i].(type) {
				case int:
					val = float64(v)
				case int64:
					val = float64(v)
				case float64:
					val = v
				default:
					panic(fmt.Sprintf("bad field %s with value type: %T", fieldName, v))
				}

				labels := []prompb.Label{
					{Name: labels.MetricName, Value: string(p.MeasurementName)},
					{Name: "measurement", Value: string(fieldName)},
					{Name: string(devops.MachineTagKeys[0]), Value: string(host.Name)},
					{Name: string(devops.MachineTagKeys[1]), Value: string(host.Region)},
					{Name: string(devops.MachineTagKeys[2]), Value: string(host.Datacenter)},
					{Name: string(devops.MachineTagKeys[3]), Value: string(host.Rack)},
					{Name: string(devops.MachineTagKeys[4]), Value: string(host.OS)},
					{Name: string(devops.MachineTagKeys[5]), Value: string(host.Arch)},
					{Name: string(devops.MachineTagKeys[6]), Value: string(host.Team)},
					{Name: string(devops.MachineTagKeys[7]), Value: string(host.Service)},
					{Name: string(devops.MachineTagKeys[8]), Value: string(host.ServiceVersion)},
					{Name: string(devops.MachineTagKeys[9]), Value: string(host.ServiceEnvironment)},
				}
				if len(h.appendLabels) > 0 {
					labels = append(labels, h.appendLabels...)
				}

				allSeries = append(allSeries, prompb.TimeSeries{
					Labels: labels,
					Samples: []prompb.Sample{
						{
							Value:     val,
							Timestamp: now.UnixMilli(),
						},
					},
				})
			}
		}
		hostValues[string(host.Name)] = allSeries
	}
	return hostValues, nil

}

// utilities to write timeseries to the remote store
func writer(
	generator HostsSimulator,
	progressBy time.Duration,
	remotePromClient *Client,
	remotePromBatchSize int,
	headers map[string]string,
	logger *zap.Logger,
) {
	numWorkers := 3
	workers := make(chan struct{}, numWorkers)
	for i := 0; i < numWorkers; i++ {
		workers <- struct{}{}
	}

	ticker := time.NewTicker(progressBy)
	defer ticker.Stop()

	for range ticker.C {
		series, err := generator.Generate()
		if err != nil {
			logger.Fatal("error generating load", zap.Error(err))
		}

		select {
		case w := <-workers:
			go func() {
				for _, s := range series {
					remoteWrite(s, remotePromClient, remotePromBatchSize, headers, logger)
				}
				workers <- w // return the instance
			}()
		default: // all workers busy
		}
	}
}

func remoteWrite(
	series []prompb.TimeSeries,
	remotePromClient *Client,
	remotePromBatchSize int,
	headers map[string]string,
	logger *zap.Logger,
) {
	i := 0
	for ; i < len(series)-remotePromBatchSize; i += remotePromBatchSize {
		remoteWriteBatch(series[i:i+remotePromBatchSize], remotePromClient, headers, logger)
	}
	remoteWriteBatch(series[i:], remotePromClient, headers, logger)
}

func remoteWriteBatch(
	series []prompb.TimeSeries,
	remotePromClient *Client,
	headers map[string]string,
	logger *zap.Logger,
) {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	request := &prompb.WriteRequest{Timeseries: series}
	data, err := request.Marshal()
	if err != nil {
		logger.Error("error marshalling write request", zap.Error(err))
		return
	}

	encoded := snappy.Encode(nil, data)
	err = remotePromClient.Send(ctx, headers, encoded)
	if err != nil {
		logger.Error("error writing to the remote store", zap.Error(err))
		return
	}
}

// Prometheus Client
type Client struct {
	url     string
	client  *http.Client
	timeout time.Duration
	logger  *zap.Logger
}

func NewClient(url string, timeout time.Duration, logger *zap.Logger) (*Client, error) {
	httpClient, err := config.NewClientFromConfig(config.HTTPClientConfig{}, "remote_storage")
	if err != nil {
		return nil, err
	}
	return &Client{
		url:     url,
		client:  httpClient,
		timeout: timeout,
		logger:  logger,
	}, nil
}

var (
	userAgent = fmt.Sprintf("Prometheus/%s", version.Version)
)

func (c *Client) Send(ctx context.Context, headers map[string]string, data []byte) error {
	request, err := http.NewRequest("POST", c.url, bytes.NewReader(data))
	if err != nil {
		return err
	}

	request.Header.Add("Content-Encoding", "snappy")
	request.Header.Add("Content-Type", "application/x-protobuf")
	request.Header.Add("User-Agent", userAgent)
	request.Header.Set("X-Prometheus-Remote-Write-Version", "0.1.0")
	for k, v := range headers {
		request.Header.Set(k, v)
	}

	request = request.WithContext(ctx)
	response, err := c.client.Do(request)
	if err != nil {
		return err
	}

	if response.StatusCode/100 != 2 {
		scanner := bufio.NewScanner(io.LimitReader(response.Body, 256))
		var line string
		if scanner.Scan() {
			line = scanner.Text()
		}
		err = errors.Errorf("server returned HTTP status %s: %s", response.Status, line)
	}
	io.Copy(ioutil.Discard, response.Body)
	response.Body.Close()
	return err
}

func main() {
	logger, _ := zap.NewProduction()
	hostGen := NewHostsSimulator(100, time.Now(), HostSimulatorOptions{
		Labels:                    make(map[string]string),
		SyntheticLabelCardinality: 0,
	})

	url := "http://localhost:1234/write"
	client, err := NewClient(url, time.Minute, logger)
	if err != nil {
		logger.Fatal("error creating remote client", zap.Error(err))
	}

	writer(hostGen, time.Second, client, 10, map[string]string{}, logger)
}
